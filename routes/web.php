<?php

use App\Http\Controllers\Front\DashboardController;
use App\Http\Controllers\Front\EmailController;
use App\Http\Controllers\Front\HomeController;
use App\Http\Controllers\Front\UserController;
use App\Models\User;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/dashboard', [DashboardController::class, 'index'])
    ->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/user/wallets', [UserController::class, 'index'])
    ->middleware(['auth', 'verified'])->name('user.wallets');
Route::get('/user/transactions', [UserController::class, 'transactions'])
    ->middleware(['auth', 'verified'])->name('user.transactions');

Route::get('/email/verify', [EmailController::class, 'verify'])
    ->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', [EmailController::class, 'emailVerification'])
    ->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification',[EmailController::class, 'sendNotificationEmail'])
    ->middleware(['auth', 'throttle:6,1'])->name('verification.send');

require __DIR__ . '/auth.php';
