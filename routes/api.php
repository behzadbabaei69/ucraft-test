<?php

use App\Http\Controllers\API\V1\Front\ConfigController;
use App\Http\Controllers\API\V1\Front\UserWalletController;
use App\Http\Controllers\API\V1\Front\WalletTransactionController;
use App\Http\Controllers\API\V1\Front\WalletTypeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//   dd("user");
//    return $request->user();
//});
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['namespace' => 'API'], function () {

    Route::group(['prefix' => 'v1', 'namespace' => 'V1'], function () {

        Route::group(['as' => 'api.front', 'middleware' => ['auth:sanctum'], 'prefix' => 'front', 'namespace' => 'Front'], function () {

            Route::group(['as' => '.config', 'prefix' => 'config'], function () {
                Route::get('/', [ConfigController::class, 'index']);
            });

            Route::group(['as' => '.user', 'prefix' => 'user'], function () {

            });

            Route::group(['prefix' => 'wallet', 'as' => '.userWallet'], function () {

                Route::get('/', [UserWalletController::class, 'index'])->name('.index');
                Route::post('/', [UserWalletController::class, 'store'])->name('.store');
                Route::delete('/{userWallet}', [UserWalletController::class, 'destroy'])->name('.delete');

            });

            Route::group(['prefix' => 'walletTransaction', 'as' => '.walletTransaction'], function () {
                Route::get('/{userWallet}', [WalletTransactionController::class, 'index'])->name('.index');
                Route::post('/', [WalletTransactionController::class, 'store'])->name('.store');
                Route::delete('/{walletTransaction}', [WalletTransactionController::class, 'destroy'])
                    ->name('.destroy');
            });

            Route::group(['prefix' => 'walletType', 'as' => '.walletType'], function () {
                Route::get('/', [WalletTypeController::class, 'index']);
            });

        });

    });

});
