<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('User Transactions') }}
        </h2>
    </x-slot>


    {{--    <main id="transaction"> </main>--}}

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Status</th>
                            <th scope="col">Type</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transactions as $transaction)
                            <tr class="{{$transaction->cssClass}}">
                                <td>
                                    {{ $transaction->id }}
                                </td>
                                <td>
                                    ${{ $transaction->amount }}
                                </td>
                                <td>
                                    {{ $transaction->statusText }}
                                </td>
                                <td>
                                    {{ $transaction->transaction_type }}
                                </td>
                            </tr>
                        @endforeach

                        <tr>
                            <td colspan="4">
                                Total Blanace ${{ Auth::user()->balance }}
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</x-app-layout>
