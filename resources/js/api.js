import axios from 'axios';

const token = 'Bearer ' + window.accessToken
export const api = axios.create({
    baseURL: `/api/v1/front/`,
    headers: {
        Authorization: token,
        Accept: 'application/json'
    }
})
