require('./bootstrap');
import Vue from 'vue/dist/vue'
import ElementUI from 'element-ui'
import "element-ui/lib/theme-chalk/index.css";
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'

import App from './App/App.vue';
import locale from 'element-ui/lib/locale/lang/en'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
Vue.use(ElementUI,{locale});

import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(Toast);

const app = new Vue({
    el: '#app',
    render: h => h(App),
});

// const transaction = new Vue({
//     el: '#transaction',
//     render: h => h(Transactions),
// });


