<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserWallet extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'user_id', 'wallet_type_id', 'status'];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function walletType(): BelongsTo
    {
        return $this->belongsTo(WalletType::class, 'wallet_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function walletTransactions()
    {
        return $this->hasMany(WalletTransaction::class, 'user_wallet_id');
    }


    public function getBalanceAttribute()
    {
        $balance = 0;
        $transactions = $this->walletTransactions()->get();
        foreach ($transactions as $transaction) {
            if ($transaction->transaction_type == WalletTransaction::TYPE_CREDIT) {
                $balance += $transaction->amount;
            } else {
                $balance -= $transaction->amount;
            }
        }
        return (float)number_format($balance,2);
    }

}
