<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletTransaction extends Model
{
    use HasFactory;

    const TYPE_CREDIT = 'credit';
    const TYPE_DEBIT = 'debit';

    const PENDING_STATUS = 0;
    const APPROVED_STATUS = 1;
    const REJECTED_STATUS = 2;

    const STATUS_TEXT = [
        self::PENDING_STATUS => 'Pending',
        self::APPROVED_STATUS => 'Approved',
        self::REJECTED_STATUS => 'Rejected'
    ];

    protected $fillable = ['amount', 'user_wallet_id', 'transaction_type', 'description', 'status'];

    protected $appends = ['statusText'];

    public function userWallet()
    {
        return $this->belongsTo(UserWallet::class, 'user_wallet_id');
    }

    public function scopeApproved(Builder $query)
    {
        return $query->where('status', self::APPROVED_STATUS);
    }

    public function getStatusTextAttribute()
    {
        return self::STATUS_TEXT[$this->status];
    }

    public function getCssClassAttribute()
    {
        return ($this->transaction_type == self::TYPE_CREDIT) ? 'table-success' : 'table-warning';

    }

}
