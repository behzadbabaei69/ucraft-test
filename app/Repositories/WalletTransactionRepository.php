<?php

namespace App\Repositories;

use App\Models\UserWallet;
use App\Models\WalletTransaction;

class WalletTransactionRepository extends Repository
{

    /**
     * WalletTransactionRepository constructor.
     * @param WalletTransaction $walletTransaction
     */
    public function __construct(WalletTransaction $walletTransaction)
    {
        $this->model = $walletTransaction;
    }

    /**
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function get($filters = [])
    {
        //doing filters here
        return $this->model->get();
    }

    /**
     * @param $userWalletId
     * @return mixed
     */
    public function findByUserWalletId($userWalletId)
    {
        //doing filters here
        return $this->model->approved()->where('user_wallet_id', $userWalletId)->get();
    }


    /**
     * @param array $data
     * @return WalletTransaction|null
     * @throws \Exception
     */
    public function store($data = [])
    {
        \DB::beginTransaction();
        try {
            //lock the last record or current user transaction

//            $query = WalletTransaction::query();
            $data['id'] = $data['id'] ?? null;

            $transaction = WalletTransaction::find($data['id']);
            //for the update record
//            $balance = $data['amount'];
            if (!$transaction) {

                $transaction = new WalletTransaction();
//                $balance = $this->getUserWalletBalance($data['user_wallet_id'], $data['id']);
            }

            //update the create a record
            $transaction->amount = $data['amount'];
            $transaction->user_wallet_id = $data['user_wallet_id'];
            $transaction->transaction_type = $data['transaction_type'];
            $transaction->description = $data['description'];
            $transaction->status = $data['status'] ?? 1;
            $transaction->save();

            \DB::commit();

            return $transaction;
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }
    }

    public function getUserWalletBalance($userWalletId, $transactionId)
    {
        $query = WalletTransaction::query();
        $transactions = $query->approved()->where('user_wallet_id', $userWalletId)
            ->where('id', '<', $transactionId)->get();
        $balance = 0;
        foreach ($transactions as $transaction) {
            if ($transaction->transaction_type == WalletTransaction::TYPE_CREDIT) {
                $balance += $transaction->amount;
            } else {
                $balance -= $transaction->amount;
            }
        }
        return $balance;
    }

}
