<?php

namespace App\Repositories;


use App\Models\WalletType;

class WalletTypeRepository extends Repository
{

    /**
     * WalletTypeRepository constructor.
     * @param WalletType $walletType
     */
    public function __construct(WalletType $walletType)
    {
        $this->model = $walletType;
    }

    /**
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function get($filters = [])
    {
        //doing filters here
        return $this->model->get();
    }


}
