<?php

namespace App\Repositories;

/**
 * Class Repository
 * @package App\Repositories
 */
abstract class Repository
{
    protected $model = null;

    public abstract function get($filters = []);
}
