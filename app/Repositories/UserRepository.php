<?php

namespace App\Repositories;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserRepository extends Repository
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $query;

    /**
     * UserRepository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
        $this->query = $this->model->query();
    }


    public function get($filters = [])
    {
        if (isset($filters['take'])) {
            $this->query->take((int)$filters['take'])->skip((int)$filters['skip']);
        }
        $this->query->orderByDesc('created_at');
        $items = $this->query->get();
        return $items;
    }

    public function findById($userId)
    {
        return $this->model->find($userId);
    }

    public function register($data = [])
    {
        $password = $data['password'];
        $user = new User();
        $user->name = $data['name'] ?? '';
        $user->email = $data['email'];
        $user->password = Hash::make($password);
        $user->save();

        return ($user) ? $user : null;
    }

    public function delete($id)
    {
        $user = User::find($id);

        if ($user) {
            $result = $user->delete();
        }

        return $result;
    }
}
