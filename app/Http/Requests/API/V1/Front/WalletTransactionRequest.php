<?php


namespace App\Http\Requests\API\V1\Front;


use Illuminate\Foundation\Http\FormRequest;

class WalletTransactionRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric',
            'user_wallet_id' => 'required|integer',
            'transaction_type' => 'required|in:credit,debit',
            'description' => 'required'
        ];
    }
}
