<?php

namespace App\Http\Controllers;

/**
 * Class FrontApiBaseController
 * @package App\Http\Controllers
 */
class FrontApiBaseController extends Controller
{
    /**
     * @param string $errorMessage
     * @return \Illuminate\Http\JsonResponse
     */
    protected function responseError($errorMessage = 'error')
    {
        return response()->json(['status' => 'error', 'message' => $errorMessage]);
    }

    /**
     * @param array $data
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function responseSuccess($data = [], $message = 'success')
    {
        return response()->json(['status' => 'success', 'message' => $message, 'data' => $data]);
    }
}
