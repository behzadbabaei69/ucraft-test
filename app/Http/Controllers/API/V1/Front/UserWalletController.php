<?php

namespace App\Http\Controllers\API\V1\Front;

use App\Http\Controllers\FrontApiBaseController;
use App\Http\Requests\API\V1\Front\UserWalletRequest;
use App\Http\Resources\V1\UserWallet as UserWalletResource;
use App\Models\UserWallet;
use App\Repositories\UserRepository;
use Exception;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserWalletController
 * @package App\Http\Controllers\API\V1\Front
 */
class UserWalletController extends FrontApiBaseController
{
    /**
     * @var UserRepository
     */
    private $userRepository;


    /**
     * UserWalletController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    /**
     * @param Request $request
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        $wallets = UserWallet::where('user_id', $user->id)->get();
        $userWallets = UserWalletResource::collection($wallets);
        return $this->responseSuccess($userWallets);
    }

    /**
     * @param UserWalletRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UserWalletRequest $request)
    {
        try {
            $user = auth()->user();

            $userWallet = UserWallet::find($request->id);
            if (!$userWallet) {
                $userWallet = new UserWallet();
            }

            $data = [
                'name' => $request->name,
                'wallet_type_id' => $request->wallet_type_id,
                'user_id' => $user->id,
            ];

            $userWallet->fill($data)->save();

            return $this->responseSuccess(
                ['wallet' => new UserWalletResource($userWallet)],
                'Your order is successfully stored!'
            );
        } catch (Exception $e) {
            \Log::error($e->getMessage().' & '.$e->getFile().' & '.$e->getLine());
            return $this->responseError("Cannot process your request at the moment!");
        }

    }


    /**
     * @param UserWallet $userWallet
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function destroy(UserWallet $userWallet)
    {
        if ($userWallet) {
            $userWallet->delete();
            $response = $this->responseSuccess([], 'Record deleted successfully!');
        } else {
            $response = $this->responseError("Record not found!");
        }

        return $response;
    }


}
