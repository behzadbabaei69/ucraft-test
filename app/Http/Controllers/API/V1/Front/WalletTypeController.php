<?php

namespace App\Http\Controllers\API\V1\Front;

use App\Http\Controllers\FrontApiBaseController;
use App\Http\Resources\V1\WalletType as WalletTypeResource;
use App\Repositories\WalletTypeRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WalletTypeController
 * @package App\Http\Controllers\API\V1\Front
 */
class WalletTypeController extends FrontApiBaseController
{
    /**
     * @var WalletTypeRepository
     */
    private $walletTypeRepository;


    /**
     * WalletTypeController constructor.
     * @param WalletTypeRepository $walletTypeRepository
     */
    public function __construct(WalletTypeRepository $walletTypeRepository)
    {
        $this->walletTypeRepository = $walletTypeRepository;
    }


    /**
     * @param Request $request
     */
    public function index(Request $request)
    {
        $walletTypes = $this->walletTypeRepository->get();
        $walletTypes = WalletTypeResource::collection($walletTypes);
        return $this->responseSuccess($walletTypes);
    }

}
