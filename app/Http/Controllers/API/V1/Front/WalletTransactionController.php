<?php

namespace App\Http\Controllers\API\V1\Front;

use App\Http\Controllers\FrontApiBaseController;
use App\Http\Requests\API\V1\Front\WalletTransactionRequest;
use App\Http\Resources\V1\WalletTransaction as WalletTransactionResource;
use App\Models\UserWallet;
use App\Models\WalletTransaction;
use App\Repositories\WalletTransactionRepository;
use Exception;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WalletTypeController
 * @package App\Http\Controllers\API\V1\Front
 */
class WalletTransactionController extends FrontApiBaseController
{
    /**
     * @var WalletTransactionRepository
     */
    private $transactionRepository;

    /**
     * WalletTransactionController constructor.
     * @param WalletTransactionRepository $transactionRepository
     */
    public function __construct(WalletTransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * @param UserWallet $userWallet
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(UserWallet $userWallet)
    {
        $transactions = $this->transactionRepository->findByUserWalletId($userWallet->id);
        $transactions = WalletTransactionResource::collection($transactions);
        return $this->responseSuccess($transactions);
    }

    /**
     * @param WalletTransactionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(WalletTransactionRequest $request)
    {
        try {
            $data = $request->all();
            $transaction = $this->transactionRepository->store($data);
            return $this->responseSuccess(
                new WalletTransactionResource($transaction),
                "Transaction is stored successfully!"
            );
        } catch (Exception $e) {
            $this->responseError("Error in processing record");
        }

    }


    /**
     * @param WalletTransaction $walletTransaction
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(WalletTransaction $walletTransaction)
    {
       try {
           if ($walletTransaction) {
               $walletTransaction->delete();
               $response = $this->responseSuccess([], 'Record deleted successfully!');
           } else {
               $response = $this->responseError("Record not found!");
           }

           return $response;
       } catch (Exception $e) {
           $this->responseError("Error in deleting record");
       }
    }


}
