<?php

namespace App\Http\Controllers\API\V1\Front;

use App\Http\Controllers\FrontApiBaseController;
use Illuminate\Http\Request;
use App\Http\Resources\V1\User as UserResource;

/**
 * Class ConfigController
 * @package App\Http\Controllers\API\V1\Front
 */
class ConfigController extends FrontApiBaseController
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        return $this->responseSuccess([
            'user' => new UserResource($user),
            'user_token' => null,
            'base_url' => '',
            'api_prefix' => '/api/v1/front/'
        ]);
    }

}
