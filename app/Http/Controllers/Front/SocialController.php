<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Exception;

/**
 * Class SocialController
 * @package App\Http\Controllers\Front
 */
class SocialController extends Controller
{
    /**
     * @param $provider
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectSocial($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * @param $provider
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function loginWithSocial($provider)
    {
        try {

            $fieldName = $provider . '_id';
            $user = Socialite::driver($provider)->user();
            $isUser = User::where($fieldName, $user->id)->first();

            if ($isUser) {
                Auth::login($isUser);
                return redirect('/dashboard');
            } else {
                $user = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    $fieldName => $user->id,
                    'password' => \Hash::make('12345678')
                ]);

                $user->email_verified_at = Carbon::now();
                $user->save();

                Auth::login($user);


                Auth::user()->tokens()->delete();
                $token = Auth::user()->createToken("api_token");
                session(['api_token' => $token->plainTextToken]);

                return redirect('/dashboard');
            }

        } catch (Exception $exception) {
//            dd($exception->getMessage());
        }
    }
}
