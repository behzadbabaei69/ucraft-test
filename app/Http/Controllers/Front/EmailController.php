<?php


namespace App\Http\Controllers\Front;


use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

class EmailController extends Controller
{

    public function verify()
    {
        return view('auth.verify-email');
    }


    public function emailVerification(EmailVerificationRequest $request)
    {
        $request->fulfill();
        return redirect('/home');
    }

    public function sendNotificationEmail(Request $request)
    {
        $request->user()->sendEmailVerificationNotification();
        return back()->with('message', 'Verification link sent!');
    }
}
