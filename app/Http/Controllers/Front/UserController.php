<?php


namespace App\Http\Controllers\Front;


use App\Http\Controllers\Controller;
use App\Models\WalletTransaction;


class UserController  extends Controller
{
    public function index()
    {
        return view('wallets');
    }

    public function transactions()
    {
        $user = auth()->user();
//        dd($user->wallets()->pluck('id')->toArray());
        $transactions = WalletTransaction::whereIn('user_wallet_id', $user->wallets()->pluck('id')->toArray())->get();
        return view('transactions', compact('user', 'transactions'));
    }

}
