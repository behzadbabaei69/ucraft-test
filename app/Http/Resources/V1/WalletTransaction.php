<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;


class WalletTransaction extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'amount' => (float)$this->amount,
            'transaction_type' => $this->transaction_type,
            'status_text' => $this->statusText,
            'user_wallet_id' => $this->user_wallet_id,
            'css_class' => $this->cssClass
        ];
    }
}
