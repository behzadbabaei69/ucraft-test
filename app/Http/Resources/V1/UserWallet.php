<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\V1\WalletType as WalletTypeResource;

class UserWallet extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'wallet_type_id' => $this->wallet_type_id,
            'wallet_type' => new WalletTypeResource($this->walletType),
            'balance' => $this->balance
        ];
    }
}
