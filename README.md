
## UCraft Test Project

#### I assume that your development environment is Ubuntu desktop or server

### Test User (is added by seed)
username: behzadbabaei69@gmai.com
password: 12345678


### Composer
Installing the required packages (in project directory)

```composer update```

Generating the application key

```php artisan key:generate```

Initializing DB

```php artisan migrate --seed```


### Hosting the project using nginx
serve the application or follow the below description to host the project using Nginx


add it to your /etc/hosts

`127.0.0.1       ucraft-test.com` 

`127.0.0.1       www.ucraft-test.com`


nginx deploy
add a file named "ucraft" in /etc/nginx/sites-available folder

```
server {
    listen 80;
    listen [::]:80;
	
    server_name  www.ucraft-test.com;

    root /var/www/ucraft/public;
    index  index.php index.html index.htm;

    location / {
        try_files $uri $uri/ /index.php?$query_string;        
    }

    location ~ \.php$ {
         try_files $uri =404;
         fastcgi_split_path_info  ^(.+\.php)(/.+)$;
         fastcgi_index            index.php;
         fastcgi_pass             unix:/var/run/php/php7.4-fpm.sock;
         include                  fastcgi_params;
         fastcgi_param   PATH_INFO       $fastcgi_path_info;
         fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }
}
```

then create a symbolic link for the config file

```
ln -s /etc/nginx/sites-available/ucraft /etc/nginx/sites-enabled/ucraft
```

### E-Mail
for purpose of testing, I'm using a Mailtrap, so for testing, you should set Mailtrap or your mail server configuration 

```
MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=25
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=
MAIL_FROM_NAME="${APP_NAME}"

```


##Tests

All the test uses the RefreshDatabase trait, so please do not forget to run the migration command after performing the tests

```php artisan migrate --seed```

```php artisan migrate:fresh --seed```

Possible test groups:

walletType => ```test-wt```
UserWallet => ```test-uw```
WalletTransaction=> ```test-t```

you can just run the group test by running the below code:

```vendor/bin/phpunit  --group={group_name}```

or simply run all the test

```php artisan test```

###Socialite Configuration
As a part of the test for the social login and registration, The package is used for this purpose is "laravel/socialite" so please provides the following configuration in the .env file 

```
GOOGLE_CLIENT_ID=
GOOGLE_CLIENT_SECRET=
GOOGLE_CALLBACK_URL=http://www.ucraft-test.com/auth/google/callback

FACEBOOK_CLIENT_ID=
FACEBOOK_CLIENT_SECRET=
FACEBOOK_CALLBACK_URL=http://www.ucraft-test.com/auth/facebook/callback
```


### Node

you can install the required  node packages by running the below code

```npm run install```
