<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalletTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_transactions', function (Blueprint $table) {
            $table->id();
            $table->decimal('amount');
            $table->unsignedBigInteger('user_wallet_id');
            $table->enum('transaction_type',['credit','debit'])->default('credit');
            $table->string('description')->nullable();
            $table->tinyInteger('status')->default(0)
                ->comment('0=PENDING | 1=APPROVED | 2=REJECTED ');
            $table->foreign('user_wallet_id')
                ->references('id')
                ->on('user_wallets')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallet_transactions');
    }
}
