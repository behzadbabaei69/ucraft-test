<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\WalletType;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(WalletTypeSeeder::class);

    }
}
