<?php

namespace Database\Seeders;

use App\Models\WalletType;
use Illuminate\Database\Seeder;

class WalletTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wallets = [
            'Credit Card',
            'Cash',
            'PayPal'
        ];

        foreach ($wallets as $key => $wallet) {
            WalletType::firstOrCreate([
                'name' => $wallet
            ]);
        }
    }
}
