<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\WalletType;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!User::where('email', 'behzadbabaei69@gmail.com')->first()) {
            $user = User::firstOrCreate([
                'name' => 'behzad babaei',
                'email' => 'behzadbabaei69@gmail.com',
                'password' => \Hash::make('12345678'),
                'email_verified_at' => Carbon::now(),
            ]);
        }
    }
}
