<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\UserWallet;
use App\Models\WalletType;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

class UserWalletTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     * @group test-uwt
     * @return void
     */
    public function test_user_wallet()
    {
        $user = User::factory()->create([
            'email_verified_at' => Carbon::now(),
        ]);

        $token = $user->createToken("api_token");
        $api_token = $token->plainTextToken;

        $walletType = WalletType::create([
            'name' => 'Cash'
        ]);

        $this->assertDatabaseHas('wallet_types', ['name' => 'Cash']);

        $response = $this->post('api/v1/front/wallet', [
            "name" => "my_credit_1",
            "wallet_type_id" => $walletType->id
        ], [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $api_token
        ]);

        $responseContent = $response->getContent();

        $this->assertNotEmpty($responseContent);
        $this->assertStringContainsString('success', $responseContent);

        $this->assertDatabaseHas('user_wallets', ['name' => 'my_credit_1']);

        $userWallet = UserWallet::where('name', 'my_credit_1')->first();

        $responseUpdate = $this->post('api/v1/front/wallet', [
            "id" => $userWallet->id,
            "name" => "my_credit_2",
            "wallet_type_id" => $walletType->id
        ], [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $api_token
        ]);

        $responseUpdateContent = $responseUpdate->getContent();
        $this->assertNotEmpty($responseUpdateContent);
        $this->assertStringContainsString('success', $responseUpdateContent);

        $this->assertDatabaseHas('user_wallets', ['name' => 'my_credit_2']);


        $responseDelete = $this->delete('api/v1/front/wallet/' . $userWallet->id, [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $api_token
        ]);

        $this->assertDatabaseMissing('user_wallets', ['name' => 'my_credit_2']);
    }
}
