<?php

namespace Tests\Feature;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class WalletTypeTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     * @group test-wt
     * @return void
     */
    public function test_wallet_type()
    {
        $user = User::factory()->create([
            'email_verified_at' => Carbon::now(),
        ]);

        $token = $user->createToken("api_token");
        $api_token = $token->plainTextToken;

        $response = $this->get('/api/v1/front/walletType', [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $api_token
        ]);

        $responseContent = $response->getContent();

        $this->assertNotEmpty($responseContent);
        $this->assertStringContainsString('success',$responseContent);


    }
}
